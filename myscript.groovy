def buildjar(){
    echo "building jar"
    sh 'mvn package'
}

def buildandpushimage(){
    echo "building image"
    sh 'docker build -t carellevbt/test-1:jenkinsfilegv .'
    sh 'echo $DOCKERHUB_CREDENTIALS_PSW | docker login -u $DOCKERHUB_CREDENTIALS_USR --password-stdin'
    sh 'docker push carellevbt/test-1:jenkinsfilegv'
}

def deployapp (){
    echo "deploying application"
}

return this