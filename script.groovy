def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: '8ec5ea6c-9923-4531-8ff0-5585794cfa87', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t carellevbt/test-1:multi-1 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push carellevbt/test-1:multi-1'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this